package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Pallol implements Veggies {
    public String toString() {
        return "Pallol";
    }
}
