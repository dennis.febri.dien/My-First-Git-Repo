package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class PallolDough implements Dough {
    public String toString() {
        return "Pallol dough is the best dough";
    }
}
