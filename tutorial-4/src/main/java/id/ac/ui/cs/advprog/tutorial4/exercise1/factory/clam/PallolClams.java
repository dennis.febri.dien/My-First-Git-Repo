package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PallolClams implements Clams {

    public String toString() {
        return "Clams from Pallol Island";
    }
}
