package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class PallolCheese implements Cheese {

    public String toString() {
        return "Pallol Cheese";
    }
}
