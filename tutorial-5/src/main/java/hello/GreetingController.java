package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name,
                                       @RequestParam(value = "visitor", defaultValue = "")
                                       String visitor, Model model) {
        model.addAttribute("name", name);
        if (visitor.equals("")) {
            model.addAttribute("title","This is my CV");
        } else {
            model.addAttribute("title",visitor + ", I hope you interested to hire me");
        }
        return "greeting";
    }

}
