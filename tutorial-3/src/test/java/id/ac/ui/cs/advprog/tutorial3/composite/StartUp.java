package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;


public class StartUp {

    public static void main(String[] args) {
        Company company = new Company();

        Ceo dennis = new Ceo("dennis", 500000.00);
        company.addEmployee(dennis);

        BackendProgrammer titi = new BackendProgrammer("titi", 200000.00);
        company.addEmployee(titi);

        FrontendProgrammer ridwan = new FrontendProgrammer("ridwan",66000.00);
        company.addEmployee(ridwan);

        System.out.println(company.getNetSalaries());
    }
}