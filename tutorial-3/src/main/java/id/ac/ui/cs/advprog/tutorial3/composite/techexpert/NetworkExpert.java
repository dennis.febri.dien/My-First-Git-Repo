package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    
    public NetworkExpert(String name, double salary) {
        if (salary >= 50000) {
            this.name = name;
            this.salary = salary;
            this.role = "Network Expert";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 50000");
        }
    }
    
    @Override
    public double getSalary() {
        return salary;//TODO Implement
    }
}
