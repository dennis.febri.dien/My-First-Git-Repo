package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        if (salary >= 100000) {
            this.name = name;
            this.salary = salary;
            this.role = "CTO";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 100000");
        }
    }

    @Override
    public double getSalary() {
        return salary; //TODO Implement
    }
}
