package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;//BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;//FillingDecorator;


public class Restourant {

    public static void main(String[] args) {
        Food thickBunBurgerSpecial;
        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());
    }
}