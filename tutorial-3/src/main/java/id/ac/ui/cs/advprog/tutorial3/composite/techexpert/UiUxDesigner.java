package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    
    public UiUxDesigner(String name, double salary) {
        if (salary >= 90000) {
            this.name = name;
            this.salary = salary;
            this.role = "UI/UX Designer";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 90000");
        }
    }

    @Override
    public double getSalary() {
        return salary;//TODO Implement
    }
}
