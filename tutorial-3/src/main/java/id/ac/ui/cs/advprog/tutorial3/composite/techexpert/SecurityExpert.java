package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    
    public SecurityExpert(String name, double salary) {
        if (salary >= 70000) {
            this.name = name;
            this.salary = salary;
            this.role = "Security Expert";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 70000");
        }
    }

    @Override
    public double getSalary() {
        return salary;//TODO Implement
    }
}
