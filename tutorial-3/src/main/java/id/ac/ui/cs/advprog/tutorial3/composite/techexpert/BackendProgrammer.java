package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) {
        if (salary >= 20000) {
            this.name = name;
            this.salary = salary;
            this.role = "Back End Programmer";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 20000");
        }
    }

    @Override
    public double getSalary() {
        return salary;//TODO Implement
    }
}
