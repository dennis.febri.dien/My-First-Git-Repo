package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        if (salary >= 200000) {
            this.name = name;
            this.salary = salary;
            this.role = "CEO";
        } else {
            throw new IllegalArgumentException("salary has to more or equal than 200000");
        }
    }

    @Override
    public double getSalary() {
        return salary; //TODO Implement
    }
}
